
# Elastic Container Registry
# Notable here is that image_tag_mutability is set to be MUTABLE. 
# This is necessary in order to put a latest tag on the most recent image.

resource "aws_ecr_repository" "ecr_repository" {
  name                 = "${var.project_name}-ecr"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }  

  tags   = {
    Name = "${var.project_name}-ecr"
  }  
}

# lifecycle policy, to make sure I don’t keep too many versions of image, 
# as with every new deployment of the application, a new image would be created.
#
resource "aws_ecr_lifecycle_policy" "ecr_lifecycle_policy" {
  repository = aws_ecr_repository.ecr_repository.name
 
  policy = jsonencode({
   rules = [{
     rulePriority = 1
     description  = "keep last 10 images"
     action       = {
       type = "expire"
     }
     selection     = {
       tagStatus   = "any"
       countType   = "imageCountMoreThan"
       countNumber = 10
     }
   }]
  })
}