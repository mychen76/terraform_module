resource "aws_transit-gateway" "transit_gateway" {
  name        = "tgw-internet"
  description = "TGW internet for other VPCs"
  enable_auto_accept_shared_attachments = false

  vpc_attachments = {
    vpc = {
      vpc_id       = module.vpc.vpc_id
      subnet_ids   = [module.vpc.private_subnet_az1_id,module.vpc.private_subnet_az2_id]
      dns_support  = true
      ipv6_support = false

      tgw_routes = [
        {
          destination_cidr_block = "0.0.0.0/0"
        },
        {
          blackhole = true
          destination_cidr_block = "40.0.0.0/20"
        }
      ]
    }
  }

  ram_allow_external_principals = true
  ram_principals = [307990089504]

  tags = {
    Purpose = "${var.project_name}-tgw-internet"
  }
}
