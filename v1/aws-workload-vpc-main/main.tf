data "aws_availability_zones" "availability_zones" {}
data "aws_caller_identity" "current_id" {}

data "aws_vpc_endpoint_service" "gateway_endpoint" {
  count        = var.enabled ? length(var.gateway_endpoints) : 0
  service      = var.gateway_endpoints[count.index]
  service_type = "Gateway"
}

### Create VPC ###

resource "aws_vpc" "this" {
  count                = var.enabled ? 1 : 0
  cidr_block           = var.vpc_cidr
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = merge(
    var.tags,
    tomap({ "Name" = "${var.vpc_name}-vpc" })
  )
}

### Configure VPC flow logs ###

resource "aws_flow_log" "vpc_flow_logs" {
  count                    = var.enabled ? 1 : 0
  vpc_id                   = aws_vpc.this[0].id
  traffic_type             = "ALL"
  max_aggregation_interval = "60"
  log_destination_type     = "s3"
  log_destination          = "${var.flow_log_bucket}/${data.aws_caller_identity.current_id.account_id}/"

  tags = merge(
    var.tags,
    tomap({ "Name" = "${var.vpc_name}-flow-logs" })
  )
}

### Configure DHCP options ###

resource "aws_vpc_dhcp_options" "vpc_dhcp_options" {
  count               = var.enabled ? 1 : 0
  domain_name         = var.domain_name
  domain_name_servers = var.dns_servers
  ntp_servers         = var.ntp_servers

  tags = merge(
    var.tags,
    tomap({ "Name" = "${var.vpc_name}-dhcpoptions" })
  )
}

resource "aws_vpc_dhcp_options_association" "vpc_dhcp_options_association" {
  count           = var.enabled ? 1 : 0
  vpc_id          = aws_vpc.this[0].id
  dhcp_options_id = aws_vpc_dhcp_options.vpc_dhcp_options[0].id
}

### Attach to transit gateway ###

resource "aws_ec2_transit_gateway_vpc_attachment" "tgw_attachment" {
  count              = var.enabled ? 1 : 0
  subnet_ids         = [for subnet in aws_subnet.tgw.*.id : subnet]
  transit_gateway_id = var.tgw_id
  vpc_id             = aws_vpc.this[0].id

  tags = merge(
    var.tags,
    tomap({ "Name" = "${var.vpc_name}-tgw-attach" })
  )
}

### Create gateway endpoint ###

locals {
  gateway_endpoints = flatten([
    for service in aws_vpc_endpoint.gateway_endpoint.*.id : [
      for route_table in concat(aws_route_table.private_app.*.id, aws_route_table.private_db.*.id) : {
        service     = service
        route_table = route_table
      }
    ]
  ])
}

resource "aws_vpc_endpoint" "gateway_endpoint" {
  count             = var.enabled ? length(data.aws_vpc_endpoint_service.gateway_endpoint) : 0
  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.gateway_endpoint[count.index].service_name
  vpc_endpoint_type = "Gateway"

  tags = merge(
    var.tags,
    tomap({ "Name" = format("%s-%s%s", var.vpc_name, data.aws_vpc_endpoint_service.gateway_endpoint[count.index].service, "-endpoint") })
  )
}

resource "aws_vpc_endpoint_route_table_association" "gateway_endpoint" {
  count           = var.enabled ? length(local.gateway_endpoints) : 0
  vpc_endpoint_id = local.gateway_endpoints[count.index].service
  route_table_id  = local.gateway_endpoints[count.index].route_table
}
