variable "enabled" {
  description = "Create VPC resources"
  type        = bool
  default     = false
}

variable "tags" {
  description = "Map of tags"
  type        = map(string)
  default     = {}
}

variable "vpc_name" {
  description = "VPC name (-vpc suffix will automatically be appended)"
  type        = string

  validation {
    error_message = "Valid characters are [-A-Za-z0-9] as this is also used for the firewall rule policy name which is more restrictive."
    condition     = can(regex("^[-A-Za-z0-9]+$", var.vpc_name))
  }

  validation {
    error_message = "-vpc suffix will be appended automatically, do not include as part of variable."
    condition     = !can(regex("-vpc$", var.vpc_name))
  }
}

variable "vpc_cidr" {
  description = "VPC IPv4 CIDR block"
  type        = string
}

variable "domain_name" {
  description = "DNS suffix"
  type        = string
  default     = "cloud.ea.epson.net"
}

variable "dns_servers" {
  description = "List of DNS servers assigned through DHCP"
  type        = list(string)
  default     = ["AmazonProvidedDNS"]
}

variable "ntp_servers" {
  description = "List of NTP servers assigned through DHCP"
  type        = list(string)
  default     = []
}

variable "flow_log_bucket" {
  description = "ARN of S3 bucket to send VPC flow logs"
  type        = string
}

variable "app_subnets_prefix" {
  description = "Aggregate prefix for the application subnets, which will be split across 2 AZs"
  type        = string
}

variable "db_subnets_prefix" {
  description = "Aggregate prefix for the database subnets, which will be split across 2 AZs"
  type        = string
}

variable "tgw_enabled" {
  description = "Create transit gateway subnets and attach to existing transit gateway"
  type        = bool
  default     = false
}

variable "tgw_subnets_prefix" {
  description = "Aggregate prefix for the transit gateway subnets, which will be split across 2 AZs"
  type        = string
}

variable "tgw_id" {
  description = "Transit gateway ID"
  type        = string
}

variable "gateway_endpoints" {
  type        = list(string)
  description = "List of gateway endpoints to create, e.g., 's3', 'dynamodb'"
  default     = []
}
