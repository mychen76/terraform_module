### Create subnets ###

resource "aws_subnet" "private_app" {
  count                   = var.enabled ? 2 : 0
  vpc_id                  = aws_vpc.this[0].id
  availability_zone       = data.aws_availability_zones.availability_zones.names[count.index]
  cidr_block              = cidrsubnet(var.app_subnets_prefix, 1, count.index)
  map_public_ip_on_launch = false

  tags = {
    Name = format("%s%s%s", var.vpc_name, "-app-private-", substr(data.aws_availability_zones.availability_zones.names[count.index], -1, 1))
  }
}

resource "aws_subnet" "private_db" {
  count                   = var.enabled ? 2 : 0
  vpc_id                  = aws_vpc.this[0].id
  availability_zone       = data.aws_availability_zones.availability_zones.names[count.index]
  cidr_block              = cidrsubnet(var.db_subnets_prefix, 1, count.index)
  map_public_ip_on_launch = false

  tags = {
    Name = format("%s%s%s", var.vpc_name, "-db-private-", substr(data.aws_availability_zones.availability_zones.names[count.index], -1, 1))
  }
}

resource "aws_subnet" "tgw" {
  count                   = var.enabled ? 2 : 0
  vpc_id                  = aws_vpc.this[0].id
  availability_zone       = data.aws_availability_zones.availability_zones.names[count.index]
  cidr_block              = cidrsubnet(var.tgw_subnets_prefix, 1, count.index)
  map_public_ip_on_launch = false

  tags = {
    Name = format("%s%s%s", var.vpc_name, "-tgw-", substr(data.aws_availability_zones.availability_zones.names[count.index], -1, 1))
  }
}

### Create route tables ###

resource "aws_route_table" "private_app" {
  count  = var.enabled ? 2 : 0
  vpc_id = aws_vpc.this[0].id

  tags = {
    Name = format("%s%s%s", var.vpc_name, "-app-private-", substr(data.aws_availability_zones.availability_zones.names[count.index], -1, 1))
  }
}

resource "aws_route_table" "private_db" {
  count  = var.enabled ? 2 : 0
  vpc_id = aws_vpc.this[0].id

  tags = {
    Name = format("%s%s%s", var.vpc_name, "-db-private-rt-", substr(data.aws_availability_zones.availability_zones.names[count.index], -1, 1))
  }
}

resource "aws_default_route_table" "default" {
  count                  = var.enabled ? 1 : 0
  default_route_table_id = aws_vpc.this[0].default_route_table_id

  tags = {
    Name = format("%s%s", var.vpc_name, "-default-rt")
  }
}

### Associate subnets with route tables ###

resource "aws_route_table_association" "private_app" {
  count          = var.enabled ? 2 : 0
  subnet_id      = aws_subnet.private_app[count.index].id
  route_table_id = aws_route_table.private_app[count.index].id
}

resource "aws_route_table_association" "private_db" {
  count          = var.enabled ? 2 : 0
  subnet_id      = aws_subnet.private_db[count.index].id
  route_table_id = aws_route_table.private_db[count.index].id
}

resource "aws_route_table_association" "tgw" {
  count          = var.enabled && var.tgw_enabled ? 2 : 0
  subnet_id      = aws_subnet.tgw[count.index].id
  route_table_id = aws_default_route_table.default[0].id
}
