### Create routes ###

resource "aws_route" "private_app_default" {
  count                  = var.enabled ? 2 : 0
  destination_cidr_block = "0.0.0.0/0"
  route_table_id         = aws_route_table.private_app[count.index].id
  transit_gateway_id     = var.tgw_id
}

resource "aws_route" "private_db_default" {
  count                  = var.enabled ? 2 : 0
  destination_cidr_block = "0.0.0.0/0"
  route_table_id         = aws_route_table.private_db[count.index].id
  transit_gateway_id     = var.tgw_id
}
