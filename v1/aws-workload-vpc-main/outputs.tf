output "vpc_id" {
  description = "VPC ID"
  value       = aws_vpc.this.*.id
}

output "vpc_name" {
  description = "VPC name"
  value       = aws_vpc.this[*].tags["Name"]
}

output "availability_zones" {
  description = "Availability zone names where resources are created"
  value       = var.enabled ? slice(data.aws_availability_zones.availability_zones.names, 0, 2) : null
}

output "private_app_subnet_ids" {
  description = "Private app subnet IDs"
  value       = aws_subnet.private_app.*.id
}

output "private_app_subnet_cidrs" {
  description = "Private app subnet CIDRs"
  value       = aws_subnet.private_app.*.cidr_block
}

output "private_app_subnet_names" {
  description = "Private app subnet names"
  value       = aws_subnet.private_app[*].tags["Name"]
}

output "private_app_rtb_ids" {
  description = "Private app route table IDs"
  value       = aws_route_table.private_app.*.id
}

output "private_db_subnet_ids" {
  description = "Private database subnet IDs"
  value       = aws_subnet.private_db.*.id
}

output "private_db_subnet_cidrs" {
  description = "Private database subnet CIDRs"
  value       = aws_subnet.private_db.*.cidr_block
}

output "private_db_subnet_names" {
  description = "Private database subnet names"
  value       = aws_subnet.private_db[*].tags["Name"]
}

output "private_db_rtb_ids" {
  description = "Private database route table IDs"
  value       = aws_route_table.private_db.*.id
}

output "tgw_subnet_ids" {
  description = "Transit gateway subnet IDs"
  value       = aws_subnet.tgw.*.id
}

output "tgw_subnet_cidrs" {
  description = "Transit gateway subnet CIDRs"
  value       = aws_subnet.tgw.*.cidr_block
}

output "tgw_subnet_names" {
  description = "Transit gateway subnet names"
  value       = aws_subnet.tgw[*].tags["Name"]
}

output "tgw_rtb_id" {
  description = "Transit gateway (default) route table ID"
  value       = aws_default_route_table.default.*.id
}

output "gateway_endpoints" {
  description = "Gateway endpoint names"
  value       = aws_vpc_endpoint.gateway_endpoint[*].tags["Name"]
}

output "vpc_attachment_id" {
  description = "Transit gateway VPC attachment ID"
  value       = aws_ec2_transit_gateway_vpc_attachment.tgw_attachment.*.id
}
