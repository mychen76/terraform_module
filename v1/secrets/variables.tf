variable "project_name" {
  description = "the name of your stack, e.g. \"demo\""
}

variable "application-secrets" {
  description = "A map of secrets that is passed into the application. Formatted like ENV_VAR = VALUE"
  type        = map
}
