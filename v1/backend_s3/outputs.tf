output "s3_bucket_name" {
    value = aws_s3_bucket.terraform-state-storage-s3.id
}

output "dynamodb_table_name" {
    value = aws_dynamodb_table.dynamodb-terraform-state-lock.name
}
