# terraform state file setup
# create an S3 bucket to store the state file in
resource "aws_s3_bucket" "terraform-state-storage-s3" {
  bucket = "${var.project_name}-terraform-backend-s3"
  lifecycle {
    prevent_destroy = false
  }
  acl = "private"
  tags      = {
    Name    = "${var.project_name} S3 Remote Terraform State Store"
  }  
}

# # terraform state file
# resource "aws_s3_bucket_acl" "terraform_state_acl" {
#   bucket = aws_s3_bucket.terraform-state-storage-s3.id
#   acl    = "private"
# }

# terraform state file
resource "aws_s3_bucket_versioning" "terraform_state_versioning" {
  bucket = aws_s3_bucket.terraform-state-storage-s3.id
  versioning_configuration {
    status = "Disabled"
  }
}

# create a dynamodb table for locking the state file
resource "aws_dynamodb_table" "dynamodb-terraform-state-lock" {
  name = "${var.project_name}-dynamodb-state-lock"
  hash_key = "LockID"
  read_capacity = 20
  write_capacity = 20
  attribute {
    name = "LockID"
    type = "S"
  }
  tags      = {
    Name    = "${var.project_name} DynamoDB Terraform State Lock Table"
  }    
}
