# Elastic Container Service

## create kms key
resource "aws_kms_key" "ecs_kms_key" {
  description             = "ecs_kms_key"
  deletion_window_in_days = 10
}

## create cloudwatch log group
resource "aws_cloudwatch_log_group" "ecs_log_group" {
  name = "${var.project_name}-ecs-cluster"
  lifecycle {
    create_before_destroy = true
  }
}

## create ecs cluster with log configuration 

resource "aws_ecs_cluster" "ecs_cluster" {
  name = "${var.project_name}-cluster"
  setting {
    name  = "containerInsights"
    value = "disabled"
  }

  configuration {
    execute_command_configuration {
      kms_key_id = aws_kms_key.ecs_kms_key.arn
      logging    = "OVERRIDE"
      log_configuration {
        cloud_watch_encryption_enabled = true
        cloud_watch_log_group_name     = aws_cloudwatch_log_group.ecs_log_group.name
      }
    }
  }
}

# # create task definition
# resource "aws_ecs_task_definition" "task" {
#   family                   = "${var.project_name}-task-definition"
#   execution_role_arn       = var.ecs_tasks_execution_role_arn
#   network_mode             = "awsvpc"
#   requires_compatibilities = ["FARGATE"]
#   cpu                      = 256
#   memory                   = 256

#   runtime_platform {
#     operating_system_family = "LINUX"
#     cpu_architecture        = "X86_64"
#   }

#   container_definitions = jsonencode([
#     {
#       name      = "${var.project_name}-container"
#       image     = "${var.container_image}:latest"
#       essential = true

#       portMappings = [
#         {
#           protocol      = "tcp"
#           containerPort = var.container_port
#           hostPort      = var.container_port
#         }
#       ]

#       ulimits = [
#         {
#           name      = "nofile",
#           softLimit = 1024000,
#           hardLimit = 1024000
#         }
#       ]

#       logConfiguration = {
#         logDriver = "awslogs",
#         options = {
#           "awslogs-group"         = "${var.project_name}-logs-group"
#           "awslogs-region"        = var.region
#           "awslogs-stream-prefix" = "task_"
#         }
#       }
#     }
#   ])
# }

# # Provides an ECS task set - effectively a task that is expected to run until an error occurs or a user terminates it 
# # (typically a webserver or a database).
# # resource "aws_ecs_task_set" "taskset" {
# #   service         = aws_ecs_service.example.id
# #   cluster         = aws_ecs_cluster.example.id
# #   task_definition = aws_ecs_task_definition.example.arn

# #   load_balancer {
# #     target_group_arn = aws_lb_target_group.example.arn
# #     container_name   = "nginx"
# #     container_port   = 8080
# #   }
# # }

# # create ecs service
# resource "aws_ecs_service" "ecs_service" {
#   name            = "${var.project_name}-service"
#   launch_type     = "FARGATE"
#   cluster         = aws_ecs_cluster.ecs_cluster.id
#   task_definition = aws_ecs_task_definition.test_task.arn
#   # platform_version  =  
#   desired_count                      = var.service_desired_count
#   deployment_minimum_healthy_percent = 50
#   deployment_maximum_percent         = 200

#   # task tagging configuration
#   #enable_ecs_managed_tags            = 
#   #propagate_tags                     = 

#   # vpc and security groups
#   network_configuration {
#     subnets          = [var.private_app_subnet_az1_id, var.private_app_subnet_az2_id]
#     security_groups  = [var.alb_security_group_id, var.ecs_security_group_id]
#     assign_public_ip = false
#   }

#   # load balancing
#   load_balancer {
#     target_group_arn = var.alb_target_group_arn
#     container_name   = "${var.project_name}-container"
#     container_port   = var.container_port
#   }

#   lifecycle {
#     ignore_changes = [task_definition, desired_count]
#   }

# }


# # Autoscaling
# # multiple rules on when to scale the number of tasks, namely based on either memory usage or cpu utilization. 

# resource "aws_appautoscaling_target" "ecs_target" {
#   max_capacity       = 4
#   min_capacity       = 1
#   resource_id        = "service/${aws_ecs_cluster.ecs_cluster.name}/${aws_ecs_service.ecs_service.name}"
#   scalable_dimension = "ecs:service:DesiredCount"
#   service_namespace  = "ecs"
# }

# resource "aws_appautoscaling_policy" "ecs_policy_memory" {
#   name               = "memory-autoscaling"
#   policy_type        = "TargetTrackingScaling"
#   resource_id        = aws_appautoscaling_target.ecs_target.resource_id
#   scalable_dimension = aws_appautoscaling_target.ecs_target.scalable_dimension
#   service_namespace  = aws_appautoscaling_target.ecs_target.service_namespace

#   target_tracking_scaling_policy_configuration {
#     predefined_metric_specification {
#       predefined_metric_type = "ECSServiceAverageMemoryUtilization"
#     }

#     target_value = 80
#   }
# }

# resource "aws_appautoscaling_policy" "ecs_policy_cpu" {
#   name               = "cpu-autoscaling"
#   policy_type        = "TargetTrackingScaling"
#   resource_id        = aws_appautoscaling_target.ecs_target.resource_id
#   scalable_dimension = aws_appautoscaling_target.ecs_target.scalable_dimension
#   service_namespace  = aws_appautoscaling_target.ecs_target.service_namespace

#   target_tracking_scaling_policy_configuration {
#     predefined_metric_specification {
#       predefined_metric_type = "ECSServiceAverageCPUUtilization"
#     }

#     target_value = 60
#   }
# }
