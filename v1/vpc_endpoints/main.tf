# create s3 endpoints 
data "aws_vpc_endpoint_service" "s3" {
  service = "s3"
  filter {
    name   = "service-type"
    values = ["Gateway"]
  }
}

resource "aws_vpc_endpoint" "endpoint_s3" {
  vpc_id       = var.vpc_id
  service_name = "${data.aws_vpc_endpoint_service.s3.service_name}"
  route_table_ids = var.route_table_ids
  tags = {
    Name = "${var.project_name}-endpoint_s3"
  }  
}

# resource "aws_vpc_endpoint_route_table_association" "private_s3" {
#   vpc_endpoint_id = data.aws_vpc_endpoint.vpc_endpoint_s3.id
#   route_table_id  = aws_route_table.route_table_ids
# }

# resource "aws_security_group" "vpc_endpoint_security_group" {
#   name        = "vpc_endpoint_security_group"
#   description = "Allow VPC traffic to communicate with AWS Services"
#   vpc_id      = var.vpc_id

#   ingress {
#     from_port   = 443
#     to_port     = 443
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#  tags = {
#     Name = "${var.project_name}-vpc_endpoint_security_group"
#   }

# }

# resource "aws_vpc_endpoint" "vpc_endpoint_ssm" {
#   vpc_id            = var.vpc_id
#   service_name      = "com.amazonaws.${var.region}.ssm"
#   vpc_endpoint_type = "Interface"
#   subnet_ids        = ["${var.subnet_ids}"]

#   security_group_ids = [
#     "${aws_security_group.vpc_endpoint_security_group.id}"
#   ]
#   # private_dns_enabled = true
# }
